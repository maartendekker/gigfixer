﻿using GigFixer.LogicLayer.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace GigFixer.DataLayer
{
    class ApplicationSQLContext : IApplicationContext
    {
        private static string connString = "Server=mssql.fhict.local;Database=dbi407381;User Id=dbi407381;Password=Password1234;";

        public bool CheckValidLogin(string email, string password)
        {
            string query = "SELECT * FROM individueel.[User] WHERE email = @email AND CONVERT(VARCHAR, password) = @password";

            SqlConnection conn = new SqlConnection(connString);
            SqlCommand command = new SqlCommand(query);
            command.Parameters.AddWithValue("@email", email);
            command.Parameters.AddWithValue("@password", password);
            command.Connection = conn;

            conn.Open();
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                conn.Close();
                return true;
            }
            else
            {
                conn.Close();
                return false;
            }
        }
    }
}
