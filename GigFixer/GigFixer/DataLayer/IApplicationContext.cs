﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GigFixer.DataLayer
{
    public interface IApplicationContext
    {
        bool CheckValidLogin(string email, string password);
    }
}
