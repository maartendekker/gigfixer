﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GigFixer.Classes
{
    public class User
    {
        private int id;
        private string password;
       
        public User(string emailAddress, string firstName, string lastName, string infix, string telNr)
        {
            this.EmailAddress = emailAddress;
            this.FirstName = firstName;
            this.LastName = lastName;
            this.Infix = infix;
            this.TelNr = telNr;
        }

        public string EmailAddress { get; }
        public string FirstName { get; }
        public string LastName { get; }
        public string Infix { get; }
        public string TelNr { get; }
        public string FullName { get; }
    }

      

}

