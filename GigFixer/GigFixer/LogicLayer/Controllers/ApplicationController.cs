﻿using GigFixer.LogicLayer.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Threading.Tasks;
using GigFixer.DataLayer;
using GigFixer.Classes;

namespace GigFixer.LogicLayer.Controllers
{
    public class ApplicationController
    {
        public static User LoggedInUser;
        public bool Login(string emailaddress, string password)
        {
            ApplicationSQLContext applicationSQLContext = new ApplicationSQLContext();
            ApplicationRepository applicationRepository = new ApplicationRepository(applicationSQLContext);
            

            if (applicationRepository.CheckValidLogin(emailaddress, password) == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


    }
}
