﻿using GigFixer.Classes;
using GigFixer.DataLayer;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GigFixer.LogicLayer.Repositories
{
    public class ApplicationRepository
    {
        readonly IApplicationContext _context;

        public ApplicationRepository(IApplicationContext context)
        {
            _context = context;
        }

        private static ApplicationRepository instance;
        private string connString;

        public ApplicationRepository()
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

            this.connString = this.connString = "Server=mssql.fhict.local;Database=dbi407381;User Id=dbi407381;Password=Password1234;";
        }

        internal bool CheckValidLogin(string emailaddress, string password)
        {
            return _context.CheckValidLogin(emailaddress, password);
        }

        internal static User PullAccount()
        {
            throw new NotImplementedException();
        }

        public static ApplicationRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ApplicationRepository();
                }
                return Instance;
            }
        }
            /// <param name="query"></param>
        public User PullAccount(string query)
        {
            SqlConnection conn = new SqlConnection(connString);
            SqlCommand command = new SqlCommand(query)
            {
                Connection = conn
            };

            conn.Open();
            SqlDataReader reader = command.ExecuteReader();
            reader.Read();

            //Save all values from reader in variables
            string emailAddress = reader["email"].ToString();
            string firstName = reader["firstName"].ToString();
            string infix = reader["infix"].ToString();
            string lastName = reader["lastName"].ToString();
            string telNr = reader["telNr"].ToString();
            
            
            SqlConnection connection = new SqlConnection(connString);
           
            connection.Open();
            
            //create a new user object for the entire project and add the variables
            //so they can be used in the rest of the program
            User user = new User(emailAddress, firstName, lastName, infix, telNr);

            connection.Close();
            conn.Close();
            return user;
        }

        private List<User> GetUsers()
        {
            string query = "" +
                "SELECT [User].email, [User].firstName, [User].lastName, [User].infix, [User].telNr, [User].RoleId " +
                "FROM proftaak.[User]; ";

            SqlConnection conn = new SqlConnection(connString);
            SqlCommand command = new SqlCommand(query)
            {
                Connection = conn
            };

            conn.Open();
            SqlDataReader reader = command.ExecuteReader();

            List<User> users = new List<User>();

            while (reader.Read())
            {
                users.Add(new User
                    (
                        reader["email"].ToString(),
                        reader["firstName"].ToString(),
                        reader["lastName"].ToString(),
                        reader["infix"].ToString(),
                        reader["telNr"].ToString()
                    )
                );
            }

            if (users.Count > 0)
            {
                return users;
            }

            return null;
        }

    }
}
