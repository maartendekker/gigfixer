﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GigFixer.LogicLayer.Controllers;

namespace GigFixer.Forms
{
    public partial class LoginScreen : Form
    {
        public LoginScreen()
        {
            InitializeComponent();
        }

        private void BtnSubmit_Click(object sender, EventArgs e)
        {
            string email = TxtBoxEmail.Text.Trim();
            string password = TxtBoxPassword.Text.Trim();

            ApplicationController applicationController = new ApplicationController();

            bool command = applicationController.Login(email, password);

            if (command == true)
            {
                this.Hide();
                MainScreen main = new MainScreen(applicationController);
                main.Show();
            }
            else
            {
                MessageBox.Show("Try Again");
                TxtBoxPassword.Clear();
            }
        }
    }
}
